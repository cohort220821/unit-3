## String functions
# https://www.w3schools.com/python/python_ref_string.asp
# split
my_name = "Alan Delima"
name_list = my_name.split(" ")
print(name_list)

# join
my_cars = ["Toyota Camry", "Lexus cth200", "Honda Civic"]
my_cars_with_commas = ", ".join(my_cars)
print(my_cars_with_commas)


# reverse
letters = ["A", "B", "C"]
letters.reverse()
print(letters)











##########################
import datetime

print(datetime.date(2003, 7, 1))
print(datetime.date.today())
print(datetime.date.today() + datetime.timedelta(days=3))




##########################
from datetime import datetime, timedelta

print(datetime.date(2003, 7, 1))
print(datetime.date.today())
print(datetime.date.today() + timedelta(days=3))