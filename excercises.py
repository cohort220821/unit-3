
# Count the number of words in this paragraph.
paragraph = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'

# Put your code here








# join
date = ["2022", "08", "01"]

# Print the above as a string
# Output should be 08/01/2022




# While loop
# Using a while loop, create a program where you ask the persons name
# The program will continue to ask a persons name until you type "exit"






# Using a for loop, print this pattern below.  Implement a nested for loop (a for loop inside a for loop)
# You can use a for loop and if statements
# Fist row are 6 asterisks
# Last row are 6 asterisks
# Total of 6 rows altogether

"""
******
*    *
*    *
*    *
*    *
******

"""














## Calculate earnings of a movie
movie_data = [
    {
        "date": "2022-08-05",
        "day": "Friday",
        "earnings": 2033045
    },
    {
        "date": "2022-08-06",
        "day": "Saturday",
        "earnings": 1806350
    },
    {
        "date": "2022-08-07",
        "day": "Sunday",
        "earnings": 1607735
    },
    {
        "date": "2022-08-08",
        "day": "Monday",
        "earnings": 540555
    },
    {
        "date": "2022-08-09",
        "day": "Tuesday",
        "earnings": 750975
    },
    {
        "date": "2022-08-10",
        "day": "Wednesday",
        "earnings": 431105
    },
    {
        "date": "2022-08-11",
        "day": "Thursday",
        "earnings": 389805
    },
    {
        "date": "2022-08-12",
        "day": "Friday",
        "earnings": 673545
    },
    {
        "date": "2022-08-13",
        "day": "Saturday",
        "earnings": 963880
    },
    {
        "date": "2022-08-14",
        "day": "Sunday",
        "earnings": 770530
    },
    {
        "date": "2022-08-15",
        "day": "Monday",
        "earnings": 223950
    },
    {
        "date": "2022-08-16",
        "day": "Tuesday",
        "earnings": 368460
    },
    {
        "date": "2022-08-17",
        "day": "Wednesday",
        "earnings": 211255
    },
    {
        "date": "2022-08-18",
        "day": "Thursday",
        "earnings": 165420
    },
]


# Given data above
# Print the date, the day, the earnings and the cumulative earnings








